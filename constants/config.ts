import {StoreDataConfig} from "../stores/store";

export const default_store: StoreDataConfig = {
    settings: {
        position: 'left',
        title: 'Settings',
        items: [
            {
                title: 'Data refresh period',
                code: 'refresh',
                current: '5',
                available: ['1', '3', '5', '10', '20', '30', '60', '100']
            }
        ]
    },
    rates: {
        position: 'right',
        title: 'Rates',
        items: [
            {
                'from': 'EUR',
                'to': 'USD',
                'price': '0.00',
                'update': ''
            },
            {
                'from': 'EUR',
                'to': 'RUB',
                'price': '0.00',
                'update': ''
            },
        ],
        captions: ['Symbol', 'Price']
    },
    symbols: {
        position: 'left',
        title: 'Symbols',
        items: [
            {
                from: 'EUR',
                to: 'USD',
                added: true
            },
            {
                from: 'EUR',
                to: 'RUB',
                added: true
            },
            {
                from: 'USD',
                to: 'RUB',
                added: false
            }
        ],
        currency_list: [
            {
                value: 'EUR',
                title: 'Euro',
                from_select: true,
                to_select: false,
            },
            {
                value: 'USD',
                title: 'Us Dollar',
                from_select: false,
                to_select: true,
            },
            {
                value: 'RUB',
                title: 'Ruble',
                from_select: false,
                to_select: false,
            },
            {
                value: 'JPY',
                title: 'Japanese Yen',
                from_select: false,
                to_select: false,
            },
            {
                value: 'GBP',
                title: 'Great Britain Pound',
                from_select: false,
                to_select: false,
            },
            {
                value: 'CAD',
                title: 'Canadian Dollar',
                from_select: false,
                to_select: false,
            },
        ]
    }
};

export const key_store = "main";
