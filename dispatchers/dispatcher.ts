
export interface DispatchConfig {
    key: string,
    response?: {
        from?: string,
        to?: string,
        value?: string,
        price?: string,
        update?: number,
    }
}

export class Dispatcher {

    private static _instance:Dispatcher = new Dispatcher();
    private watchers: Array<any> = [];

    constructor() {
        if(Dispatcher._instance){
            throw new Error("Error: Instantiation failed: Use Dispatcher.getInstance() instead of new.");
        }
        Dispatcher._instance = this;
    }

    public static getInstance():Dispatcher
    {
        return Dispatcher._instance;
    }

    public register(watcher: any) {
        this.watchers.push(watcher);
    }

    public unRegister(watcher: any) {
        let index = this.watchers.indexOf(watcher);
        if (index > -1) {
            this.watchers.splice(index, 1);
        }
    }

    public dispatch(data: DispatchConfig) {
        for(let key in this.watchers) {
            let watcher = this.watchers[key];
            watcher(data);
        }
    }
}

