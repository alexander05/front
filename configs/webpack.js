var webpack = require('webpack');

module.exports = {
    entry: './app.ts',
    output: {
        filename: './src/app.js'
    },
    devtool: 'eval-source-map',
    resolve: {
        extensions: ['', '.ts']
    },
    module: {
        loaders: [
            { loader: 'awesome-typescript-loader' },
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin(),
    ]
};