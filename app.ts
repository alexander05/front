import {Component} from "./components/component"
import {Dispatcher} from "./dispatchers/dispatcher";
import {Store} from "./stores/store";

Dispatcher.getInstance().register(function (data) {

    switch (data.key) {

        case 'init-store':

            Store.init().change();
            break;

        case 'change-state-api':

            Store.isAllowedSetRate(data.response) &&
            Store.setRateItem(data.response) &&
            Store.changePrice();
            break;

        case 'add-pair':

            Store.setActivePair({
                from: data.response.from,
                to: data.response.to,
                added: false
            }).addRate(data.response.from, data.response.to) &&
            Store.changePairs();
            break;

        case 'remove-pair':

            Store.setDeactivePair({
                from: data.response.from,
                to: data.response.to,
                added: true
            }).removeRate(data.response.from, data.response.to) &&
            Store.changePairs();
            break;

        case 'from-currency-select-update':

            Store.setFromCurrency(data.response.value).changeCurrencySelect();
            break;

        case 'to-currency-select-update':

            Store.setToCurrency(data.response.value).changeCurrencySelect();
            break;

        case 'change-delay':

            Store.setDelay(data.response.value);
            break;
    }

});

(new Component()).run();

