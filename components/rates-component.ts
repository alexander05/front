import {ComponentInterface} from "./component"
import {Action} from "../actions/action";

export interface ItemsRatesConfig {
    from: string,
    to: string,
    price: string,
    update: string,
}

export class RatesComponent implements ComponentInterface{

    constructor(private action: Action, private items: Array<ItemsRatesConfig>) {

    }

    public render(): HTMLDivElement {
        var content = document.createElement('div');
        content.classList.add('table');
        content.id = "table-rates";

        let row = document.createElement('div');
        row.classList.add('table-row');
        row.classList.add('caption');

        for (let item of ['Symbol', 'Price', '']) {
            let col = document.createElement('div');
            col.classList.add('table-col');
            let span = document.createElement('span');
            span.textContent = item;
            col.appendChild(span);
            row.appendChild(col);
        }
        content.appendChild(row);


        for (let item of this.items) {
            let pair = item.from + item.to;
            let row = document.createElement('div');
            row.classList.add('table-row');
            row.classList.add('subscribed');

            let col1 = document.createElement('div');
            col1.classList.add('table-col');
            let span1 = document.createElement('span');
            span1.classList.add('subscribed-pair');
            span1.textContent = pair;
            col1.appendChild(span1);
            row.appendChild(col1);

            let col2 = document.createElement('div');
            col2.classList.add('table-col');
            let span2 = document.createElement('span');
            span2.id = pair.toLowerCase() + '-price';
            if (item['price']) {
                span2.textContent = item['price'];
            } else {
                span2.appendChild(this.createAnimationGif());
            }
            col2.appendChild(span2);
            row.appendChild(col2);

            let col3 = document.createElement('div');
            col3.classList.add('table-col');
            let btn = document.createElement('input');
            btn.classList.add('btn');
            btn.type = 'button';
            btn.value = 'Remove';
            btn.dataset['from'] = item.from;
            btn.dataset['to'] = item.to;

            let action = this.action;
            btn.onclick = function () {
                action.removePair(this);
            };
            col3.appendChild(btn);

            row.appendChild(col3);
            content.appendChild(row);
        }
        return content;
    }

    private createAnimationGif() {
        let img = document.createElement('img');
        img.src = 'img/loading.gif';
        return img;
    }
}