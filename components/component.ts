import {SettingsComponent} from "./settings-component"
import {SymbolsComponent} from "./symbols-component";
import {RatesComponent} from "./rates-component";
import {Store} from "../stores/store";
import {Action} from "../actions/action";
import {ErrorComponent} from "./error-component";

export interface ComponentInterface {
    render(): HTMLDivElement;
}

const leftPosition = 'left';
const rightPosition = 'right';

export class Component {
    private state: any;
    private action: Action;

    constructor() {
        this.action = new Action();
    }

    public run() {

        var component = this;

        if (!Store.getData()) {
            document.addEventListener('changed-store', function () {
                component.render();
            });
            this.action.initStore();
        } else {
            component.render()
        }

        document.addEventListener('changed-store-rates-price', function () {
            component.updatePrice();
        });

        document.addEventListener('changed-store-pairs', function () {
            component.state = Store.getData();
            let tableSymbols = document.getElementById('table-symbols');
            tableSymbols.parentNode.replaceChild(component.renderContentComponent('symbols'), tableSymbols);
        });

        document.addEventListener('changed-store-rates', function () {
            component.state = Store.getData();
            let tableRates = document.getElementById('table-rates');
            tableRates.parentNode.replaceChild(component.renderContentComponent('rates'), tableRates);
        });
    }

    public renderContentComponent(key: string): HTMLDivElement {
        let component: ComponentInterface;
        switch (key) {
            case 'settings':
                component = new SettingsComponent(
                    this.action,
                    this.state[key]['items']
                );
                break;
            case 'rates':
                component = new RatesComponent(
                    this.action,
                    this.state[key]['items']
                );
                break;
            case 'symbols':
                component = new SymbolsComponent(
                    this.action,
                    Store.getSymbolsItems(),
                    Store.getSymbolsCurrencyList(),
                );
                break;
            default:
                component = new ErrorComponent();
        }
        return component.render();
    }

    /**
     * Get position of component for page
     *
     * @param key
     * @returns {string}
     */
    private getPosition(key: string): string {
        return this.state[key]['position'];
    }

    /**
     * Render title of component
     *
     * @param key
     * @returns {HTMLParagraphElement|HTMLElement}
     */
    public renderTitle(key: string): HTMLParagraphElement {
        let title = document.createElement('p');
        title.classList.add('title');
        title.textContent = this.state[key]['title'];
        return title;
    }

    /**
     * Render content of component
     *
     * @param key
     * @returns {HTMLDivElement|HTMLElement}
     */
    public renderContent(key: string): HTMLDivElement {
        return this.renderContentComponent(key);
    }

    private updatePrice() { // todo: mb movie to rates component?
        let ratesItems = Store.getRatesItems();
        for (let item of ratesItems) {
            if (item.price === "") {
                continue;
            }
            let id = (item.from + item.to).toLowerCase() + "-price";
            if (document.getElementById(id) !== null) {
                document.getElementById(id).textContent = item.price;
            }
        }
    }
    /**
     *  Render all components
     */
    private render() {
        this.state = Store.getData();

        let container = document.createElement('div');

        let h1 = document.createElement('h1');
        h1.textContent = 'Realtime exchange rates';
        container.appendChild(h1);

        let divLeftPart = document.createElement('div');
        divLeftPart.classList.add('part');
        divLeftPart.classList.add('left');

        let divRightPart = document.createElement('div');
        divRightPart.classList.add('part');
        divRightPart.classList.add('right');

        for (let key in this.state) {
            let block = document.createElement('div');
            block.classList.add('block');
            block.classList.add(key);
            block.appendChild(this.renderTitle(key));
            block.appendChild(this.renderContent(key));

            switch (this.getPosition(key)) {
                case leftPosition:
                    divLeftPart.appendChild(block);
                    break;
                case rightPosition:
                    divRightPart.appendChild(block);
                    break;
                default:
                    divLeftPart.appendChild(block);
                    break;
            }

        }

        container.appendChild(divLeftPart);
        container.appendChild(divRightPart);

        var action = this.action;
        document.addEventListener('DOMContentLoaded', function () {
            document.body.appendChild(container);
            action.initSubscribe((document.getElementsByClassName('subscribed-pair')));
        });
    }

}