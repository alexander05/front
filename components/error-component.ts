import {ComponentInterface} from "./component"

export class ErrorComponent implements ComponentInterface {

    constructor() {

    }

    public render(): HTMLDivElement {
        console.warn('Component not found');
        return new HTMLDivElement();
   }
}