import {ComponentInterface} from "./component"
import {Action} from "../actions/action";


export interface ItemsSymbolsConfig {
    from: string,
    to: string,
    added: boolean,
}

export interface CurrencySymbolsConfig {
    value: string,
    title: string,
    from_select: boolean,
    to_select: boolean,
}


export class SymbolsComponent implements ComponentInterface {

    constructor(private action: Action, private items: Array<ItemsSymbolsConfig>, private currency: Array<CurrencySymbolsConfig>) {

    }

    public render(): HTMLDivElement {
        var content = document.createElement('div');
        content.classList.add('table');
        content.id = "table-symbols";

        for (let item of this.items) {

            let row = document.createElement('div');
            row.classList.add('table-row');

            let col1 = document.createElement('div');
            col1.classList.add('table-col');
            let span1 = document.createElement('span');
            span1.textContent = item.from + item.to;
            col1.appendChild(span1);
            row.appendChild(col1);

            let col2 = document.createElement('div');
            col2.classList.add('table-col');
            let btn = document.createElement('input');
            btn.classList.add('btn');
            if (item['added']) {
                btn.classList.add('disabled');
            }
            btn.type = 'button';
            btn.value = 'Add';

            let action = this.action;
            btn.onclick = function () {
                action.makeActivePair(this);
            };
            btn.dataset['from'] = item.from;
            btn.dataset['to'] = item.to;
            col2.appendChild(btn);
            row.appendChild(col2);

            content.appendChild(row);
        }

        let row = document.createElement('div');
        row.classList.add('table-row');
        row.classList.add('add-item');

        let col1 = document.createElement('div');
        col1.classList.add('table-col');
        col1.appendChild(this.makeSelect(this.currency, false));
        let separator = document.createTextNode(' / ');
        col1.appendChild(separator);
        col1.appendChild(this.makeSelect(this.currency, true));
        row.appendChild(col1);

        let col2 = document.createElement('div');
        col2.classList.add('table-col');
        let btn = document.createElement('input');
        btn.classList.add('btn');

        if (this.isAddedPairFromToSelects()) {
            btn.classList.add('disabled');
        }

        btn.type = 'button';
        btn.value = 'Add';

        let action = this.action;
        btn.onclick = function () {
            action.addPair(this,
                (<HTMLInputElement>document.getElementById('from_select')).value,
                (<HTMLInputElement>document.getElementById('to_select')).value,
            );
        };
        col2.appendChild(btn);
        row.appendChild(col2);
        content.appendChild(row);
        return content
    }

    private isAddedPairFromToSelects(): boolean {

        let indexFromSelected = this.currency.findIndex((e: CurrencySymbolsConfig) => e.from_select === true);
        let indexToSelected = this.currency.findIndex((e: CurrencySymbolsConfig) => e.to_select === true);

        let from = this.currency[indexFromSelected].value;
        let to = this.currency[indexToSelected].value;

        let indexItems = this.items.findIndex((e: ItemsSymbolsConfig) => (e.from === from) && (e.to === to) && (e.added == true));

        return indexItems !== -1 ? true : false;
    }

    private makeSelect(currency: Array<CurrencySymbolsConfig>, second: boolean): HTMLSelectElement {

        let select = document.createElement('select');
        select.id = second ? 'to_select' : 'from_select';

        let action = this.action;

        select.onchange = second ?
            function () {
                action.toCurrencySelectUpdate(this.value)
            } :
            function () {
                action.fromCurrencySelectUpdate(this.value)
            };

        for (let item of currency) {

            if (second && item.from_select) {
                continue;
            }

            let option = document.createElement('option');
            option.selected = second ? item.to_select : item.from_select;
            option.value = item.value;
            option.textContent = item.title;
            select.appendChild(option);

        }

        return select;

    }

}