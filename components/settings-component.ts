import {ComponentInterface} from "./component"
import {Action} from "../actions/action";

export interface ItemsSettingsConfig {
    title: string,
    current: string,
    code: string,
    available: Array<string>
}


interface makeSelectOptionConfig {
    list: Array<string>,
    selected: string,
}


export class SettingsComponent implements ComponentInterface {

    constructor(private action: Action, private items: Array<ItemsSettingsConfig>) {

    }

    public render(): HTMLDivElement {
        let content = document.createElement('div');
        content.classList.add('table');

        for (let key in this.items) {
            let item = this.items[key];
            let row = document.createElement('div');
            row.classList.add('table-row');
            row.id = item.code;

            let col1 = document.createElement('div');
            col1.classList.add('table-col');
            let span = document.createElement('span');
            span.textContent = item['title'];
            col1.appendChild(span);
            row.appendChild(col1);

            let col2 = document.createElement('div');
            col2.classList.add('table-col');
            col2.appendChild(this.renderSelect({
                'list': item['available'],
                'selected': item['current'],
            }));
            row.appendChild(col2);

            content.appendChild(row);
        }
        return content;
    }

    private renderSelect(option: makeSelectOptionConfig ): HTMLSelectElement {
        let {selected = '', list = []} = option;

        let select = document.createElement('select');

        let action = this.action;
        select.onchange = function () {
            action.changeDelay(this.value)
        };

        for (let item of list) {
            let option = document.createElement('option');
            option.selected = item == selected;
            option.value = item;
            option.textContent = item + " second";
            select.appendChild(option);
        }
        return select;
    }
}