import {default_store, key_store} from "../constants/config";
import {isEquivalent} from "../helper";

import {ItemsRatesConfig} from "../components/rates-component";
import {ItemsSettingsConfig} from "../components/settings-component";
import {CurrencySymbolsConfig, ItemsSymbolsConfig} from "../components/symbols-component";

export interface StoreDataConfig {
    settings: {
        position: string,
        title: string,
        items: Array<ItemsSettingsConfig>
    },
    rates: {
        position: string,
        title: string,
        items: Array<ItemsRatesConfig>
        captions: Array<string>
    },
    symbols: {
        position: string,
        title: string,
        items: Array<ItemsSymbolsConfig>,
        currency_list: Array<CurrencySymbolsConfig>
    }
}

interface StoreConfig {
    data: StoreDataConfig,
    init(): StoreConfig,
    setData(data: StoreDataConfig): StoreConfig,
    getData(): StoreDataConfig,
    getSettingsItems(): Array<ItemsSettingsConfig>,
    setSettingsItems(items: Array<ItemsSettingsConfig>): StoreConfig,
    getRatesItems(): Array<ItemsRatesConfig>,
    setRatesItems(items: Array<ItemsRatesConfig>): StoreConfig,
    setRateItem(item: ItemsRatesConfig): boolean,
    getSymbolsItems(): Array<ItemsSymbolsConfig>,
    setSymbolsItems(items: Array<ItemsSymbolsConfig>): StoreConfig,
    getSymbolsCurrencyList(): Array<CurrencySymbolsConfig>,
    setSymbolsCurrencyList(list: Array<CurrencySymbolsConfig>): StoreConfig,
    setActivePair(item: ItemsSymbolsConfig): StoreConfig,
    setDeactivePair(item: ItemsSymbolsConfig): StoreConfig,
    addRate(from: string, to: string): boolean,
    removeRate(from: string, to: string): boolean,
    setDelay(value: string): void,
    isAllowedSetRate(data: ItemsRatesConfig): boolean,
    setFromCurrency(value: string): StoreConfig,
    setToCurrency(value: string): StoreConfig,
    change(): void,
    changePrice(): void,
    changePairs(): void,
    changeCurrencySelect(): void,

}
export var Store: StoreConfig = {

    data: null,

    init: function (): StoreConfig {
        Store.data = default_store;
        return Store.setData(Store.data);
    },

    setData: function (data: StoreDataConfig): StoreConfig {
        Store.data = data;
        sessionStorage.setItem(key_store, JSON.stringify(Store.data));
        return Store;
    },

    getData: function (): StoreDataConfig {

        if (Store.data !== null) {
            return Store.data;
        }

        Store.data = JSON.parse(sessionStorage.getItem(key_store));

        if (Store.data !== null) {
            return Store.data;
        }

        return Store.init().getData();
    },

    getSettingsItems: function (): Array<ItemsSettingsConfig> {
        return Store.getData().settings.items;
    },

    setSettingsItems: function (items: Array<ItemsSettingsConfig>): StoreConfig {
        let store = Store.getData();
        store.settings.items = items;
        return Store.setData(store);
    },

    getRatesItems: function (): Array<ItemsRatesConfig> {
        return Store.getData().rates.items;
    },

    setRatesItems: function (items: Array<ItemsRatesConfig>): StoreConfig {
        let store = Store.getData();
        store['rates']['items'] = items;
        return Store.setData(store);
    },

    getSymbolsItems: function (): Array<ItemsSymbolsConfig> {
        return Store.getData().symbols.items;
    },

    setSymbolsItems: function (items: Array<ItemsSymbolsConfig>): StoreConfig {
        let store = Store.getData();
        store.symbols.items = items;
        return Store.setData(store);
    },

    getSymbolsCurrencyList: function (): Array<CurrencySymbolsConfig> {
        return Store.getData().symbols.currency_list;
    },

    setSymbolsCurrencyList: function (list: Array<CurrencySymbolsConfig>): StoreConfig {
        let store = Store.getData();
        store.symbols.currency_list = list;
        return Store.setData(store);
    },

    setActivePair: function (item: ItemsSymbolsConfig): StoreConfig {
        let symbolsItems = Store.getSymbolsItems();

        let index = symbolsItems.findIndex((e: ItemsSymbolsConfig) => isEquivalent(e, item));

        item.added = true;

        if (index === -1) {
            symbolsItems.push(item);
        } else {
            symbolsItems[index] = item;
        }
        return Store.setSymbolsItems(symbolsItems);
    },

    setDeactivePair: function (item: ItemsSymbolsConfig): StoreConfig {
        let symbolsItems = Store.getSymbolsItems();

        let index = symbolsItems.findIndex((e: ItemsSymbolsConfig) => isEquivalent(e, item));

        if (index !== -1) {
            item.added = false;
            symbolsItems[index] = item;
            Store.setSymbolsItems(symbolsItems);
        }

        return Store;
    },

    addRate: function (from: string, to: string): boolean {

        let ratesItems = Store.getRatesItems();

        for (let item of ratesItems) {
            if (item.from == from && item.to == to) {
                return false;
            }
        }

        ratesItems.push({
            'from': from,
            'to': to,
            'price': '',
            'update': ''
        });

        Store.setRatesItems(ratesItems);
        return true;
    },

    removeRate: function (from: string, to: string): boolean {

        let ratesItems = Store.getRatesItems(),
            key = 0, remove = false;

        for (key; ratesItems.length; key++) {
            let item = ratesItems[key];
            if (item.from == from &&
                item.to == to) {
                remove = true;
                break;
            }
        }

        if (remove) {
            ratesItems.splice(key, 1);
            Store.setRatesItems(ratesItems);
        }
        return remove;
    },

    setDelay: function (value: string): void {

        let itemsSetting = Store.getSettingsItems();
        let indexItemSetting = itemsSetting.findIndex((e: ItemsSettingsConfig) => e.code === 'refresh');

        itemsSetting[indexItemSetting].current = value;

        Store.setSettingsItems(itemsSetting);
    },

    isAllowedSetRate: function (data: ItemsRatesConfig): boolean {

        let itemsSetting = Store.getSettingsItems();

        let refreshTime;
        for (let item of itemsSetting) {
            if (item.code == 'refresh') {
                refreshTime = item.current;
            }
        }

        let delay = parseInt(refreshTime) * 1000;
        let ratesItems = Store.getRatesItems();

        let indexRateItem = ratesItems.findIndex((e: ItemsRatesConfig) => (e.from === data.from) && (e.to === data.to));
        if (indexRateItem === -1) {
            return true;
        }

        let item = ratesItems[indexRateItem];

        if (item.update === '') {
            return true;
        }

        if ((+item.update + +delay) > +data.update) {
            return false;
        }

        console.info(':' + (new Date).getSeconds() + ' - ' + data.from + data.to);
        return true;
    },

    setRateItem: function (item: ItemsRatesConfig): boolean {
        var ratesItems = Store.getRatesItems();

        let index = ratesItems.findIndex((e: ItemsRatesConfig) => (e.from === item.from) && (e.to === item.to));

        if (index !== -1) {
            ratesItems[index] = item;
        }

        Store.setRatesItems(ratesItems);
        return true;
    },

    setFromCurrency: function (value: string): StoreConfig {
        let currencyList = Store.getSymbolsCurrencyList();

        let indexOldFromSelected = currencyList.findIndex((e: CurrencySymbolsConfig) => e.from_select === true);
        currencyList[indexOldFromSelected].from_select = false;

        let indexNewFromSelected = currencyList.findIndex((e: CurrencySymbolsConfig) => e.value === value);
        currencyList[indexNewFromSelected].from_select = true;

        Store.setSymbolsCurrencyList(currencyList);

        let indexNewToSelected = currencyList.findIndex((e: CurrencySymbolsConfig) => e.from_select === false);
        return Store.setToCurrency(Store.getSymbolsCurrencyList()[indexNewToSelected].value);
    },

    setToCurrency: function (value: string): StoreConfig {
        let currencyList = Store.getSymbolsCurrencyList();

        let indexOldToSelected = currencyList.findIndex((e: CurrencySymbolsConfig) => e.to_select === true);
        currencyList[indexOldToSelected].to_select = false;

        let indexNewToSelected = currencyList.findIndex((e: CurrencySymbolsConfig) => e.value === value);
        currencyList[indexNewToSelected].to_select = true;

        return Store.setSymbolsCurrencyList(currencyList);
    },

    change: function () {
        document.dispatchEvent(new CustomEvent("changed-store"));
    },

    changePrice: function () {
        document.dispatchEvent(new CustomEvent("changed-store-rates-price"));
    },

    changePairs: function () {
        document.dispatchEvent(new CustomEvent("changed-store-pairs"));
        document.dispatchEvent(new CustomEvent("changed-store-rates"));
    },

    changeCurrencySelect: function () {
        document.dispatchEvent(new CustomEvent("changed-store-pairs"));
    }

};


