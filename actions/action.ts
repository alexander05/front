import {Dispatcher} from "../dispatchers/dispatcher";

const connection = io('http://prices-server-mock.dmitrypodgorniy.com/');

export class Action {

    constructor() {

    }

    public initSubscribe(subscribers: HTMLCollectionOf<Element>) {

        for (let key in subscribers) {
            let item = subscribers[key];
            if (item.textContent) {
                this.subscribeRequestApi(item.textContent);
            }
        }
    }

    public initStore() {

        Dispatcher.getInstance().dispatch({
            'key': 'init-store',
        })

    }

    public makeActivePair(obj: HTMLInputElement) {

        if (obj.classList.contains('disabled')) {
            return;
        }

        let pair = obj.dataset['from'] + obj.dataset['to'];

        this.subscribeRequestApi(pair);

        Dispatcher.getInstance().dispatch({
            'key': 'add-pair',
            'response': {
                from: obj.dataset['from'],
                to: obj.dataset['to']
            }
        });

    }

    public addPair(obj: HTMLInputElement, from: string, to: string) {

        if (obj.classList.contains('disabled')) {
            return;
        }

        if (!(from + to)) {
            return;
        }

        this.subscribeRequestApi(from + to);

        Dispatcher.getInstance().dispatch({
            'key': 'add-pair',
            'response': {from: from, to: to}
        });

    }

    public removePair(obj: HTMLInputElement) {

        let pair = obj.dataset['from'] + obj.dataset['to'];

        this.unSubscribeRequestApi(pair);

        Dispatcher.getInstance().dispatch({
            'key': 'remove-pair',
            'response': {
                from: obj.dataset['from'],
                to: obj.dataset['to'],
            }
        });

    }

    public fromCurrencySelectUpdate(value: string) {

        Dispatcher.getInstance().dispatch({
            'key': 'from-currency-select-update',
            'response': {value: value}
        });

    }

    public toCurrencySelectUpdate(value: string) {

        Dispatcher.getInstance().dispatch({
            'key': 'to-currency-select-update',
            'response': {value: value}
        });

    }

    public changeDelay(value: string) {

        Dispatcher.getInstance().dispatch({
            'key': 'change-delay',
            'response': {value: value}
        });

    }

    public changeStateApi() {

        connection.on('price-change', function (data: {pair: string, price: string}) {

            Dispatcher.getInstance().dispatch({
                'key': 'change-state-api',
                'response': {
                    from: data.pair.slice(0, 3).toUpperCase(),
                    to: data.pair.slice(3, 6).toUpperCase(),
                    price: data.price,
                    update: Date.now(),
                }
            });

        })
    }

    private subscribeRequestApi(pair: string) {

        var uid = Math.random();
        console.info(pair + ' to subscribe request');

        connection.emit('subscribe-req', {
            pair: pair.toLowerCase(),
            uid: uid
        });

        this.changeStateApi();  // todo:  make change after subscribe res
    }

    private unSubscribeRequestApi(pair: string) {

        var uid = Math.random();
        console.info(pair + ' to unsubscribe request');

        connection.emit('unsubscribe-req', {
            pair: pair.toLowerCase(),
            uid: uid
        });
    }
}